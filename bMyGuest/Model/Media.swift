//
//  Media.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 22/05/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class Media: NSObject {
    
    var id : Int?
    var file : URL?
    var visibility = 0
    var categoryId = 0
    var image : UIImage?

    init(json : [String:Any]) {
        super.init()
        
        self.id = json["id"] as? Int
        self.file = URL(string: json["file"] as! String)
        self.visibility = json["visibility"] as! Int
        self.categoryId = json["categoryId"] as! Int
        
        downloadImage(url: file!)
    }
    
    init(id : Int, file : URL, visibility: Int, categoryId: Int){
        super.init()
        
        self.id = id
        self.file = file
        self.visibility = visibility
        self.categoryId = categoryId
        
        downloadImage(url: file)
    }
    
    override var description: String {
        return  "file : " + (self.file?.description)!
        +       "\n\tvisibility : " + self.visibility.description

    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { print(error!); return }
            DispatchQueue.main.async() { () -> Void in
                self.image = UIImage(data: data)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
