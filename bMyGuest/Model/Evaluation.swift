//
//  Evaluation.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 22/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class Evaluation: NSObject {
    var id : Int?
    var note : Double?
    var comment : String?
    var userId : String?
    
    
    init(json : [String: Any]){
        self.id = json["id"] as! Int
        self.note = json["note"] as! Double
        self.comment = json["comment"] as! String
        self.userId = json["userId"] as! String
    }
}
