//
//  Event.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import CoreLocation

public protocol EventDelegate : class {
    func pictureDownloaded(image: UIImage)
    
    func publicAddressFound(publicAddress : String)
}

class Event: NSObject {

    weak var delegate: EventDelegate?
    
    var id = 0
    var name_ = "No Name"
    var description_ = ""
    var address = ""
    var publicAddress = ""
    var capacity_ = 0
    var start_ = Date()
    var end_ = Date()
    var createdAt_ = Date()
    var updatedAt_ = Date()
    var latitude_ = 0.0
    var longitude_ = 0.0
    var recurrence_ = 0
    var visibility_ = 0
    var evaluations_ = [Float]()
    var picture_ : UIImage? //Media
    var pictureMedia_ : Media?
    var medias_ = [Any]()
    var tags_ = [String:Any]() //Tag
    var owner_ = User()
    var participants_ : [User] = []
    
    init(json : [String:Any]){
        super.init()
        
        let dateformatter = AppManager.sharedInstance.dateformatter
        
        self.id = json["id"] as! Int
        self.name_ = json["name"] as! String
        if let decript = Data(base64Encoded: json["description"] as! String){
            self.description_ = String(data: decript, encoding: .utf8)!
        }
        self.capacity_ = json["capacity"] as! Int
        self.start_ = dateformatter.date(from: json["start"] as! String)!
        self.end_ = dateformatter.date(from: json["end"] as! String)!
        self.createdAt_ = dateformatter.date(from: json["createdAt"] as! String)!
        self.updatedAt_ = dateformatter.date(from: json["updatedAt"] as! String)!
        self.latitude_ = Double(json["latitude"] as! String)!
        self.longitude_ = Double(json["longitude"] as! String)!
        if let jsonOwner = json["owner"] as? [String: Any]{
            self.owner_ = User(json: jsonOwner)
        }
        
        if let peoples = json["participants"] as? [[String: Any]]{
            for people in peoples{
                participants_.append(User(json: people))
            }
        }
        
        
        let media = Media(json: json["picture"] as! [String : Any])
        self.pictureMedia_ = media
        self.getAddress(lat: latitude_, lng: longitude_)
        if let file = media.file{
            downloadImage(url: file)
        }
        
        //self.recurrence_ = json["reccurence"] as! Bool
        self.visibility_ = json["visibility"] as! Int
    }
    
    func getAddress(lat : Double, lng: Double){
        let location = CLLocation(latitude: lat, longitude: lng)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            
            if let pm = placemarks?.first{
                if let addressDictionary = pm.addressDictionary{
                    let formattedAddressLines = addressDictionary["FormattedAddressLines"] as! [String]
                    self.address = formattedAddressLines[0] + ", " + formattedAddressLines[1]
                    self.publicAddress = formattedAddressLines[1]
                    if let aDelegate = self.delegate {
                        aDelegate.publicAddressFound(publicAddress: self.publicAddress)
                    }
                }
            }else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { print(error!); return }
            DispatchQueue.main.async() { () -> Void in
                self.picture_ = UIImage(data: data)
                if let aDelegate = self.delegate {
                    aDelegate.pictureDownloaded(image: self.picture_!)
                }
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    override var description: String {
        return  "\nname : " + self.name_
        +       "\ndescription : " + self.description_
        +       "\ncapacity : " + self.capacity_.description
        +       "\nstart : " + self.start_.description
        +       "\nend : " + self.end_.description
        +       "\npicture [" + self.picture_!.description + "]"
    }

}
