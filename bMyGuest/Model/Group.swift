//
//  Group.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 14/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class Group: NSObject {
    var id : Int?
    var name : String?
    var desc : String?
    var isOpen : Bool?
    var visibility = 1
    var picture : Media?
    var pictureId  : Int?
    var image : UIImage?
    var owner : User?
    var ownerId : String?
    
    var members : [User] = []
    var medias : [Media] = []
   
    init(id: Int,name: String, desc: String, picture: Media) {
        super.init()
        self.id = id
        self.name = name
        self.desc = desc
        self.picture = picture
        downloadImage(url: picture.file!)
    }
    
    init(json: [String: Any]){
        super.init()
        
        self.id = json["id"] as? Int
        self.name = json["name"] as? String
        if let decript = Data(base64Encoded: json["description"] as! String){
            self.desc = String(data: decript, encoding: .utf8)!
        }
        self.isOpen = json["isOpen"] as? Bool
        self.visibility = json["visibility"] as! Int
        self.pictureId = json["pictureId"] as? Int
        self.ownerId = json["ownerId"] as? String
        
        if let medias = json["medias"] as? [[String: Any]]{
            for media in medias{
                self.medias.append(Media(json: media))   
            }
        }
        
        if let owner = json["owner"] as? [String: Any]{
            self.owner = User(json: owner)
        }
        
        if let members = json["members"] as? [[String: Any]]{
            for member in members{
                self.members.append(User(json: member))
            }
        }
        
        if let picture = json["picture"] as? [String: Any]{
            self.picture = Media(json: picture)
            self.downloadImage(url: (self.picture?.file)!)
        }
    }

    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { print(error!); return }
            DispatchQueue.main.async() { () -> Void in
                self.image = UIImage(data: data)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
}

