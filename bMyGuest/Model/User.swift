
//
//  User.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 04/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire


class User: NSObject {
    
    var id : String?
    var token : String?
//    var firstname : String?
//    var lastname : String?
    var name : String?
    var email : String?
    var birthday : String?
    var gender : String?
    var phone : String?
    var address : String?
    var isPremium : Bool?
    var note : Double?
    var picture : String?
    var location : String?
    var medias : [Media] = []
    var contacts : [User] = []
    var events : [Event] = []
    var groups : [Group] = []
    var image : UIImage?
    var age : Int!
    //var tags : [Tag]?
    var evaluations : [Evaluation] = []
    var memberships : [Group] = []
    var participations : [Event] = []
    
    override init() {
        self.id = "10213615308206510"
        self.token = "EAARSNh6XbyIBAClcvDzTSq0A4uj2HAnZC1U14wrIYMMEsb4wF9ZBKztutrlSkZBDjjwDZAJ8JMC4iR9IJ3e3D498SBJpXaTlaJEXTb1LuY25hPFvaVbo03tiZAUKZCVgAli9AJCSEp8QDoOmZBdt1gwh9OYk5vgfKP1iAEcoWk7swvrUBVKXWRFQaUqSJYclyQ1Q59Dj8uv0uEeBXXfWncDGWeFhj6mnvpOxTEX26RhVwZDZD"
        self.name = "Alexandre Ménielle"
        self.email = "xenox76@hotmail.fr"
        self.birthday = "09/28/1995"
        self.gender = "Male"
        self.location = ""
        self.isPremium = false
        self.note = 8
        self.picture = "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/14993351_10211210101077835_5971449631700300453_n.jpg?oh=0797ec0846b35be60e3fda506f2145a2&oe=59F860D9"
    }
    
    init(id : String, token: String, name : String, email : String, birthday : String, gender : String, location : String, isPremium : Bool, note : Double, picture : String) {
        super.init()
        
        self.id = id
        self.token = token
//        self.firstname = firstname
        self.name = name
        print(self.name!)

        self.email = email
        self.birthday = birthday
        self.gender = gender
//        self.phone = phone
//        self.address = address
        self.location = location
        self.isPremium = isPremium
        self.note = note
        self.picture = picture
        self.age = calcAge(date: birthday)
        self.downloadImage(url: URL(string: picture)!)
        if self.id == UserManager.sharedInstance.user.id{
            self.postUser()
        }
        
    }
    
    init(json : [String: Any]){
        super.init()
        
        self.id = json["id"] as? String
        self.token = json["token"] as? String
        self.name = json["name"] as? String
        self.email = json["email"] as? String
        self.birthday = json["birthday"] as? String
        self.gender = json["gender"] as? String
        self.location = json["location"] as? String
        self.picture = json["picture"] as? String
        self.isPremium = json["isPremium"] as? Bool
        
        if let contacts = json["contacts"] as? [[String: Any]]{
            for contact in contacts{
                self.contacts.append(User(json: contact))
            }
        }
        
        if let groups = json["groups"] as? [[String: Any]]{
            for group in groups{
                self.groups.append(Group.init(json: group))
            }
        }
        
        if let memberships = json["memberships"] as? [[String: Any]]{
            for membership in memberships{
                self.memberships.append(Group.init(json: membership))
            }
        }
        
        if let participations = json["participations"] as? [[String: Any]]{
            for participation in participations{
                self.participations.append(Event.init(json: participation))
            }
        }
        
        if let events = json["events"] as? [[String: Any]]{
            for event in events{
                self.events.append(Event.init(json: event))
            }
        }
      
        self.age = calcAge(date: birthday!)
        self.downloadImage(url: URL(string: picture!)!)
    }
    
    func calcAge(date: String) -> Int {
        if date.characters.count == 10{
            let birthdayDate = AppManager.sharedInstance.birthdateformatter.date(from: date)
            let calendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
            let calcAge = calendar.components(.year, from: birthdayDate!, to: Date(), options: [])
            if let age = calcAge.year{
                return age
            }
        }
        return 0
    }
    
    func postUser(){
        let url = "\(Config.baseURL)users"
        
        let signInParameters: Parameters = ["id" : self.id!,
                                            "token" : self.token!,
                                            "name" : self.name!,
                                            "email" : self.email!,
                                            "birthday" : self.birthday ?? "",
                                            "gender" : self.gender ?? "",
                                            "location" : self.location ?? "",
                                            "picture" : self.picture ?? "",
                                            "isPremium" : false,
                                            "note" : "10"]
        
        print(signInParameters)
        let headers: HTTPHeaders = [:]
        
        Alamofire.request(url, method: .post, parameters: signInParameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            
            if(response.result.isSuccess){
                print(response.result)
                self.getApiUser(profileVC: nil)
            }else{
                print(response.error ?? "")
            }
        }
    }
    
    func isFriendWith(id : String) -> Bool{
        
        for contact in self.contacts{
            if contact.id == id{
                return true
            }
        }
        return id == UserManager.sharedInstance.user.id
    }
    
    //get user infos from api
    func getApiUser(profileVC: ProfileVC?){
        var changed = false
        
        var contactIds : [String] = []
        for contact in contacts{
            contactIds.append(contact.id!)
        }
        var groupIds : [Int] = []
        for group in groups{
            groupIds.append(group.id!)
        }
        
        if let id = self.id{
            let url = "\(Config.baseURL)users/\(id)"
            
            let headers: HTTPHeaders = ["Authorization": self.token!]
            let parameters: Parameters = [:]
            
            //print(url)
            
            Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
                //print(response)
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value as? [String:Any]{
                        
                        //Charge contact
                        let contacts = data["contacts"] as! [[String: Any]]
                        self.contacts.removeAll()
                        for contact in contacts{
                            let user = User(json: contact)
                            self.contacts.append(user)
                            if !contactIds.contains(user.id!){
                                changed = true
                            }
                        }
                        if let groups = data["groups"] as? [[String: Any]]{
                            self.groups.removeAll()
                            for group in groups{
                                let group = Group.init(json: group)
                                self.groups.append(group)
                                if !groupIds.contains(group.id!){
                                    changed = true
                                }
                            }
                        }
                        if let memberships = data["memberships"] as? [[String: Any]]{
                            self.memberships.removeAll()
                            for group in memberships{
                                let group = Group.init(json: group)
                                self.memberships.append(group)
                                if !groupIds.contains(group.id!){
                                    changed = true
                                }
                            }
                        }
                        if let participations = data["participations"] as? [[String: Any]]{
                            self.participations.removeAll()
                            for participation in participations{
                                self.participations.append(Event.init(json: participation))
                            }
                        }
                        if let events = data["events"] as? [[String: Any]]{
                            self.events.removeAll()
                            for event in events{
                                self.events.append(Event.init(json: event))
                            }
                        }
                        

                        if profileVC != nil{
                            //Sure that image is loaded
                            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                                if self.image != nil{
                                    changed ? profileVC?.tableView.reloadData() : print("no changements")
                                    profileVC?.activityIndicator.stopAnimating()
                                    timer.invalidate()
                                }
                            })
                            
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {//Le temps que l'image se load... a changer
//                                
//                                changed ? profileVC?.tableView.reloadData() : print("no changements")
//                                profileVC?.activityIndicator.stopAnimating()
//                            })
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error ?? "")
                    
                    break
                    
                }
            }
        }
    }
    
    func followUser(user: User){
        let url = "\(Config.baseURL)users/\(user.id!)/contacts"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                UserManager.sharedInstance.user.contacts.append(user)
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }
    
    func unFollowUser(id: String){
        let url = "\(Config.baseURL)users/\(UserManager.sharedInstance.user.id!)/contacts/\(id)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .delete, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { print(error!); return }
            DispatchQueue.main.async() { () -> Void in
                self.image = UIImage(data: data)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    
//    override var description: String{
//        return self.id + " - " + token + " - " + name + " - " + email + " - " + birthday + " - " + gender + " - " + location
//    }
}
