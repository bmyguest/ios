
//
//  ConnectionVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 04/06/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

class ConnectionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func getUserInfos(token : String){
//        print(token)
//        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, gender, birthday, link, email, location, events, friends"]).start(completionHandler: { (connection, result, error) in
//            if (error == nil){
//                let fbDetails = result as! NSDictionary
//                
//                let id = fbDetails["id"] as! String
//                let name = fbDetails["name"] as! String
//                let email = fbDetails["email"] as! String
//                let gender = fbDetails["gender"] as! String
//                let location = fbDetails["location"] as? String
//                let picture = "http://graph.facebook.com/\(id)/picture?type=large"
//                let birthday = fbDetails["birthday"] as? String
//                
//                let user = User(id: id, token: token, name: name, email: email, birthday: birthday ?? "", gender: gender, location: location ?? "", isPremium: false, note: 7.5, picture: picture)
//                UserManager.sharedInstance.user = user
//                
//            }else{
//                print(error!)
//            }
//        })
//    }
    
    @IBAction func onSignIn(_ sender: Any) {
        
        let login = LoginManager()
        login.logIn([.email, .custom("user_birthday"), .custom("user_location")/*, .custom("user_events")*/,.userFriends], viewController: self) { (loginResult) in
            
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                print("Logged in!")
                print(accessToken)
                UserManager.sharedInstance.getUserInfos(token: accessToken.authenticationToken)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
