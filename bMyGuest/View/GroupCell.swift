//
//  GroupCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 22/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class GroupCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
