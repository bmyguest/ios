//
//  ProfileCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 14/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    var user = User()

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var selectedBg: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var addFriend: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //selectedBg.isHidden = !selected
    }
    
    func setup(user : User){
        self.user = user
        
        print(UserManager.sharedInstance.user.isFriendWith(id: self.user.id!))
        addFriend.isHidden = UserManager.sharedInstance.user.isFriendWith(id: self.user.id!)

        if let image = user.image{
            profileImageView.image = image
        }
        if let name = user.name{
            nameLabel.text = name
        }
        if let location = user.location{
            locationLabel.text = location
        }
        if user.age > 0{
            ageLabel.text = user.age.description + " ans"
            ageLabel.isHidden = false
        }else{
            ageLabel.isHidden = true
        }
    }
    
    func setup(group : Group){
        addFriend.isHidden = true
        
        if let name = group.name{
            nameLabel.text = name
        }
        if let image = group.image{
            profileImageView.image = image
        }
        
        ageLabel.isHidden = true

        //locationLabel.text = group.members.count.description + " membres"
        locationLabel.text = group.desc!
    }
    
    @IBAction func onAddFriend(_ sender: Any) {
        UserManager.sharedInstance.user.followUser(user: self.user)
        addFriend.isHidden = true
    }
    
}
