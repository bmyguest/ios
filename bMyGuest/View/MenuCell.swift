//
//  MenuCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var backgroundImage: UIView!
    @IBOutlet weak var bottomLineConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLine: UIView!
    
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(image: UIImage, name: String, info: String){
        myImage.image = image
        nameLabel.text = name
    
        if info.characters.count > 0{
            infoLabel.isHidden = false
            infoLabel.text = info
        }else{
            infoLabel.isHidden = true
        }
    }
    
}
