//
//  SearchVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 13/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.title = "Recherche"
    
        let profileButton = UIBarButtonItem(image: UIImage(named: "profile"), style: .plain, target: self, action: #selector(self.onProfileClick))
        
        self.navigationItem.rightBarButtonItem = profileButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onProfileClick(){
        present(ProfileVC(), animated: true, completion: nil)
    }
    
    @IBAction func onSearchClick(_ sender: Any) {
        self.navigationController?.pushViewController(ResultsVC(), animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
