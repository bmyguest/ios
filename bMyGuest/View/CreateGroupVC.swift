//
//  CreateGroupVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 26/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire

class CreateGroupVC: UIViewController {

    @IBOutlet weak var grouImage: UIImageView!
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var groupDescription: UITextView!
    
    let imagePicker = UIImagePickerController()
    var uploadedMedia : Media?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        groupDescription.layer.cornerRadius = 5
        groupDescription.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        groupDescription.layer.borderWidth = 1
        groupDescription.text = "Description"
        groupDescription.textColor = UIColor.lightGray.withAlphaComponent(0.5)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipeDown.direction = .down
        
        view.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func clearUI(){
        grouImage.image = #imageLiteral(resourceName: "cheers")
        grouImage.alpha = 0.6
        groupName.text = ""
        groupDescription.text = ""
    }
    
    func uploadImage(imageData: Data) {
        let urlStr = "\(Config.baseURL)medias/upload"
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        
        let URL = try! URLRequest(url: urlStr, method: HTTPMethod.post, headers: headers)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "picture", fileName: "picture.jpg", mimeType: "image/jpeg")
            
        }, with: URL) { (result) in
            print(result)
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    debugPrint(response)
                    if let json = response.value as? [String: Any]{
                        self.uploadedMedia = Media(json: json)
                    }
                }
            case .failure(let encodingError):
                self.showToast(message: "Erreur à la création")
                print(encodingError)
            }
        }
    }
    
    
    func postGroup() {
        let url = "\(Config.baseURL)/groups"
        
        var pictuerId = 0
        if let id = uploadedMedia?.id{
            pictuerId = id
        }
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = ["name": groupName.text ?? "",
                                      "description": groupDescription.text!.data(using: .utf8)?.base64EncodedString() ?? "",
                                      "open" : 1,
                                      "visibility" : 1,
                                      "pictureId" : pictuerId]
        
        //print(url)
        print(parameters)
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                self.showToast(message: "Groupe crée !")
                self.clearUI()
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                self.showToast(message: "Erreur à la création")
                break
                
            }
        }
    }
    
    @IBAction func addImage(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onCreate(_ sender: Any) {
    
        if let image = grouImage.image{
            if let imageData = UIImageJPEGRepresentation(image, 1){
                uploadImage(imageData: imageData)
                
                Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { (timer) in
                    if self.uploadedMedia != nil{
                        timer.invalidate()
                        self.postGroup()
                    }
                })
                
                return
            }
        }
        
        self.showToast(message: "Erreur à la création")
    }
}

extension CreateGroupVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0{
            groupDescription.becomeFirstResponder()
            return true
        }
        
        //dismissKeyboard()
        return true
    }
}

extension CreateGroupVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            grouImage.image = pickedImage
            grouImage.alpha = 1
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension CreateGroupVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if groupDescription.text == "Description"{
            groupDescription.text = ""
            groupDescription.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if groupDescription.text.isEmpty {
            groupDescription.text = "Description"
            groupDescription.textColor = UIColor.lightGray.withAlphaComponent(0.5)
        }
    }
}
