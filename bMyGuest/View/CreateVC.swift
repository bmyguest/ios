//
//  CreateVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 13/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces

class CreateVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeKeyboardBtn: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var eventNameTf: UITextField!
    @IBOutlet weak var capacityTf: UITextField!
    @IBOutlet weak var addressTf: UITextField!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var descriptionTv: UITextView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var addImageBtn: UIButton!
    
    var uploadedMedia : Media?
    
    var originRect : CGRect!
    
    var latitude = 0.0
    var longitude = 0.0
    
    let imagePicker = UIImagePickerController()
    
    var isChangeVC = false
    var textfields : [UITextField] = []
    
    var startDate : Date?
    var endDate : Date?
    
    var event : Event?
    
    var dateToChange = "start"
    let autocompleteController = GMSAutocompleteViewController()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.title = "Créer"
        self.tabBarItem.image = UIImage(named: "add")
        
        let valideButton = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(self.onValideEvent))
        
        self.navigationItem.rightBarButtonItem = valideButton
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        originRect = self.view.bounds
        
        //textView PLaceholder
        descriptionTv.text = "Description *"
        descriptionTv.textColor = UIColor.lightGray
        descriptionTv.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        descriptionTv.layer.borderWidth = 1
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipeDown.direction = .down
        
        view.addGestureRecognizer(swipeDown)
        
        //if an event is passed -> became changeEventVC
        if let event = self.event{
            isChangeVC = true
            self.title = "Modification"
            
            if let image = event.picture_{
                imageView.image = image
            }
            
            latitude = event.latitude_
            longitude = event.longitude_
            self.addressTf.text = event.address
            //getAddress(lat: latitude, lng: event.longitude_)
            eventNameTf.text = event.name_
            capacityTf.text = event.capacity_.description
            startDateBtn.setTitle("          Début - " + AppManager.sharedInstance.longDateTimeFormatter.string(from: event.start_), for: .normal)
            endDateBtn.setTitle("          Fin - " + AppManager.sharedInstance.longDateTimeFormatter.string(from: event.end_), for: .normal)
            startDate = event.start_
            endDate = event.end_
            descriptionTv.text = event.description_
            descriptionTv.textColor = UIColor.darkGray
        }
        
        //Image picker
        imagePicker.delegate = self
        
        //Google places
        autocompleteController.delegate = self
        
        let filter = GMSAutocompleteFilter()
        filter.country = "fr"
        autocompleteController.autocompleteFilter = filter
        
        //Add Image design
        addImageBtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        
        //Add textfields
        textfields = [eventNameTf, addressTf, capacityTf]
        //Shadow on top of pickerDate
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 3

        //Add event on keyboard show
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        //Change minutes to 0 and next hour
        let gregorian = Calendar(identifier: .gregorian)
        let now = Date()
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
        
        components.hour = components.hour! + 1
        components.minute = 0
        datePicker.date = gregorian.date(from: components)!
    }
    
    func getAddress(lat : Double, lng: Double){
        let location = CLLocation(latitude: lat, longitude: lng)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            
            if let pm = placemarks?.first{
                if let addressDictionary = pm.addressDictionary{
                    let formattedAddressLines = addressDictionary["FormattedAddressLines"] as! [String]
                    self.addressTf.text = formattedAddressLines[0] + ", " + formattedAddressLines[1]
                }
            }else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func uploadImage(imageData: Data) {
        let urlStr = "\(Config.baseURL)medias/upload"
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        
        let URL = try! URLRequest(url: urlStr, method: HTTPMethod.post, headers: headers)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "picture", fileName: "picture.jpg", mimeType: "image/jpeg")

        }, with: URL) { (result) in
            print(result)
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    debugPrint(response)
                    if let json = response.value as? [String: Any]{
                        self.uploadedMedia = Media(json: json)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillShow(){
        hidePopup()
        closeKeyboardBtn.isHidden = false
    }
    
    func cleanAll(){
        for textfield in textfields{
            textfield.text = ""
        }
        startDateBtn.titleLabel?.text = "          Date de début"
        endDateBtn.titleLabel?.text = "          Date de fin"
        descriptionTv.text = "Description *"
        descriptionTv.textColor = UIColor.lightGray
        imageView.image = #imageLiteral(resourceName: "party")
        imageView.alpha = 0.5
    }
    
    func verifInfos() -> Bool{
        
        if eventNameTf.text == ""
        || addressTf.text == ""
        || startDate == nil
        || endDate == nil
        || descriptionTv.text == "Description *"
        || descriptionTv.text == ""
        || imageView.image == nil
        {
            
            return false
        }
        
        return true
    }
    
    
    //ok button event
    @objc func onValideEvent(){
        
        if verifInfos() {
            if isChangeVC {
                putEvent()
            }else{
                postEvent()
            }
        }else{
            //showToast
            showToast(message: "Veuillez remplir les champs obligatoire *")
        }

    }
    
    func dismissKeyboard() {
        view.endEditing(true)
        hidePopup()
    }
    
    func refreshUserEventsTableView(){
        let userEventsNav = self.tabBarController?.customizableViewControllers?[2] as! UINavigationController
        if let userEventsVC = userEventsNav.viewControllers[0] as? UserEventsVC{
            userEventsVC.callEvents()
        }
    }
    
    func refreshResultsVCTableView(){
        let resultNav = self.tabBarController?.customizableViewControllers?[0] as! UINavigationController
        if let resultsVC = resultNav.viewControllers[0] as? ResultsVC{
            resultsVC.callEvents(search: "")
        }
    }
    
    func putEvent(){
        let url = "\(Config.baseURL)events/\(event!.id)"
        
        var pictureId = 0
        if let media = event?.pictureMedia_{
            pictureId = media.id!
        }
        
        let signInParameters: Parameters = ["name" : eventNameTf.text ?? "",
                                            "description" : descriptionTv.text.data(using: .utf8)?.base64EncodedString() ?? "",
                                            "start" : AppManager.sharedInstance.dateformatter.string(from: startDate!),
                                            "end" : AppManager.sharedInstance.dateformatter.string(from: endDate!),
                                            "capacity" : Int(capacityTf.text!) ?? 0,
                                            "latitude" : latitude,
                                            "longitude" : longitude,
                                            "visibility" : "1",
                                            "pictureId" : pictureId]
        
        print(signInParameters)
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        
        Alamofire.request(url, method: .put, parameters: signInParameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            
            print(response)
            if(response.result.isSuccess){
                self.refreshUserEventsTableView()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func postEvent(){
        let url = "\(Config.baseURL)events"
        
        let signInParameters: Parameters = ["name" : eventNameTf.text ?? "",
                                            "description" : descriptionTv.text.data(using: .utf8)?.base64EncodedString() ?? "",
                                            "start" : AppManager.sharedInstance.dateformatter.string(from: startDate!),
                                            "end" : AppManager.sharedInstance.dateformatter.string(from: endDate!),
                                            "capacity" : Int(capacityTf.text!) ?? 0,
                                            "latitude" : latitude,
                                            "longitude" : longitude,
                                            "visibility" : "1",
                                            "pictureId" : uploadedMedia?.id ?? 1]
        
        print(signInParameters)
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        
        Alamofire.request(url, method: .post, parameters: signInParameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            
            print(response)
            if(response.result.isSuccess){
                self.cleanAll()
                self.refreshResultsVCTableView()
                self.tabBarController?.selectedIndex = 0
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showPopup(){
        view.endEditing(true)
        bottomConstraint.constant = 0
        closeKeyboardBtn.isHidden = false
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hidePopup(){
        selectDate(button: nil)
        bottomConstraint.constant = -350
        closeKeyboardBtn.isHidden = true
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func selectDate(button : UIButton?){
        startDateBtn.backgroundColor = UIColor.clear
        endDateBtn.backgroundColor = UIColor.clear
        
        if let button = button{
            button.backgroundColor = UIColor.lightGray
            //self.view.bounds = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y + 100, width: self.view.bounds.width, height: self.view.bounds.height)
        }/*else{
            self.view.bounds = originRect
        }*/
        
    }
    
    //EVENTS
    @IBAction func addImage(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onCloseKeyboard(_ sender: Any) {
        view.endEditing(true)
        hidePopup()
    }
    
    @IBAction func onStartDate(_ sender: UIButton) {
        selectDate(button: sender)
        dateToChange = "start"
        showPopup()
    }
    
    @IBAction func onEndDate(_ sender: UIButton) {
        selectDate(button: sender)
        dateToChange = "end"
        showPopup()
    }
    
    @IBAction func onValidatePicker(_ sender: Any) {
        hidePopup()

        if dateToChange == "start"{
            startDate = datePicker.date
            startDateBtn.setTitle("          Début - " + AppManager.sharedInstance.longDateTimeFormatter.string(from: datePicker.date), for: .normal)
            if (endDateBtn.titleLabel?.text?.contains("Date"))!{
                onEndDate(endDateBtn)
            }
        }else if dateToChange == "end"{
            endDate = datePicker.date
            endDateBtn.setTitle("          Fin - " + AppManager.sharedInstance.longDateTimeFormatter.string(from: datePicker.date), for: .normal)
        }
    }
    

}

extension CreateVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag < textfields.count {
            textfields[textField.tag+1].becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        
        return true
    }
}

extension CreateVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.view.bounds = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y + 110, width: self.view.bounds.width, height: self.view.bounds.height)
        if descriptionTv.text == "Description *"{
            descriptionTv.text = ""
            descriptionTv.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.view.bounds = originRect
        if descriptionTv.text.isEmpty {
            descriptionTv.text = "Description *"
            descriptionTv.textColor = UIColor.lightGray
        }
    }
}

extension CreateVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let address = place.formattedAddress{
            self.addressTf.text = address
        }
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        self.capacityTf.becomeFirstResponder()
        print(latitude)
        print(longitude)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension CreateVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageView.image = pickedImage
            imageView.alpha = 1
            
            let imageData = UIImageJPEGRepresentation(pickedImage, 1)
            self.uploadImage(imageData: imageData!)
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
