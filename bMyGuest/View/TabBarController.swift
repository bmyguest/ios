//
//  TabBarController.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 13/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        var viewControllers = [UIViewController]()
        
        //Create all vewControllers of tabBar
        /*
        let searchVC = SearchVC()
        let searchNav = UINavigationController(rootViewController: searchVC)
        searchNav.tabBarItem.image = UIImage(named: "search")*/
        
        let resultsVC = ResultsVC()
        let resultsNav = UINavigationController(rootViewController: resultsVC)
        
        let createVC = CreateVC()
        let createNav = UINavigationController(rootViewController: createVC)
        
        let userEventsVC = UserEventsVC()
        let userEventsnav = UINavigationController(rootViewController: userEventsVC)
        
        let menuVC = MenuVC()
        let menuNav = UINavigationController(rootViewController: menuVC)
        
        viewControllers.append(resultsNav)
        viewControllers.append(createNav)
        viewControllers.append(userEventsnav)
        viewControllers.append(menuNav)
        
        self.tabBar.tintColor = AppManager.sharedInstance.mainColor
        
        self.setViewControllers(viewControllers, animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeRight))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeLeft))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        
        
    }
    
    func onSwipeRight(){
        if self.selectedIndex > 0 {
            self.selectedIndex -= 1
        }
    }
    
    func onSwipeLeft(){
        if self.selectedIndex < 3 {
            self.selectedIndex += 1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    
    func showToast(message : String) {
        let width = CGFloat(Float(message.characters.count) * 11.5)
        let toastLabel = UILabel(frame: CGRect(x: (self.view.frame.size.width/2)-(width/2), y: self.view.frame.size.height/2 - 60, width: width, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "SFUIText-Regular", size: 17)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2, delay: 1, options: .transitionCrossDissolve, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
