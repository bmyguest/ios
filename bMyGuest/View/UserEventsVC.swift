//
//  UserEventsVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 06/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire

class UserEventsVC: UIViewController {
    
    @IBOutlet weak var userEventsTableview: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var noEvents: UIView!
    var events = [Event]()
    var refreshControl: UIRefreshControl!
    var isMyEvents = true
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        if isMyEvents{
            self.title = "Mes soirées"
        }else{
            self.title = "Mes participations"
        }
        self.tabBarItem.image = UIImage(named: "settings")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if isMyEvents{
            self.title = "Mes soirées"
        }else{
            self.title = "Mes participations"
        }

        activityIndicator.hidesWhenStopped = true
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        userEventsTableview.addSubview(refreshControl)
        
        //userEventsTableview.register(UINib(nibName: "ResultsCell", bundle: nil), forCellReuseIdentifier: "ResultsCell")
        userEventsTableview.register(UINib(nibName: "EventsCell", bundle: nil), forCellReuseIdentifier: "EventsCell")
        
        if isMyEvents{
            callEvents()
        }else{
            callParticipations()
        }
    }
    
    func refresh(sender:AnyObject) {
        //refresh tableView
        if isMyEvents{
            callEvents()
        }else{
            callParticipations()
        }
    }
    
    func callParticipations(){
        if !self.refreshControl.isRefreshing{
            activityIndicator.startAnimating()
        }
        let url = "\(Config.baseURL)users/\(UserManager.sharedInstance.user.id!)/participations"
        
        Alamofire.request(url).validate().responseJSON { response in
            
            guard let responseJSON = response.result.value as? [[String: Any]] else {
                print("error retreiving events")
                self.callParticipations()
                return
            }
            
            self.events.removeAll()
            for json in responseJSON {
                let event = Event.init(json: json)
                event.delegate = self
                self.events.append(event)
            }
            
            self.noEvents.isHidden = self.events.count > 0
            
            self.refreshControl.endRefreshing()
            self.userEventsTableview.reloadData()
            self.activityIndicator.stopAnimating()
        }

    }
    
    func deleteEvent(event: Event){
        let url = "\(Config.baseURL)events/\(event.id)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        print(parameters)
        
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }
    
    func quitEvent(event: Event){
        let url = "\(Config.baseURL)events/\(event.id)/participants/\(UserManager.sharedInstance.user.id!)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        print(parameters)
        
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }

    func callEvents(){
        if !self.refreshControl.isRefreshing{
            activityIndicator.startAnimating()
        }
        let url = "\(Config.baseURL)users/\(UserManager.sharedInstance.user.id!)/events"
        
        Alamofire.request(url).validate().responseJSON { response in
            
            guard let responseJSON = response.result.value as? [[String: Any]] else {
                print("error retreiving events")
                self.callEvents()
                return
            }
            
            self.events.removeAll()
            
            for json in responseJSON {
                let event = Event.init(json: json)
                event.delegate = self
                self.events.append(event)
            }
            
            self.noEvents.isHidden = self.events.count > 0
            
            self.refreshControl.endRefreshing()
            self.userEventsTableview.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    Events
    
    @IBAction func goToCreateVC(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    
}

extension UserEventsVC : EventDelegate {
    func publicAddressFound(publicAddress: String) {
        
    }

    func pictureDownloaded(image: UIImage) {
        self.userEventsTableview.reloadData()
    }
}

extension UserEventsVC : UITableViewDataSource, UITableViewDelegate{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as! ResultsCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
        
        cell.setup(event: self.events[indexPath.row])
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isMyEvents{
            let changeVC = CreateVC()
            changeVC.event = events[indexPath.row]
            changeVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(changeVC, animated: true)
        }else{
            let eventVC = EventViewController()
            eventVC.event = events[indexPath.row]
            eventVC.hidesBottomBarWhenPushed = true
            eventVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(eventVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if isMyEvents{
        
            let deleteAction = UITableViewRowAction(style: .normal, title: "Supprimer") { (rowaction, indexPath) in

                self.events.remove(at: indexPath.row)
                tableView.reloadData()
            }
            
            deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
            
            return [deleteAction]
        }
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Quitter") { (rowaction, indexPath) in
            self.quitEvent(event: self.events[indexPath.row])
            self.events.remove(at: indexPath.row)
            tableView.reloadData()
        }
        
        deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
        
        return [deleteAction]
    }
}
