//
//  LocationAskVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import CoreLocation

protocol TutoVCDelegate {
    func locationValidate()
}

class LocationAskVC: UIViewController {

    //Constraints
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    
    var animating = false
    
    var locationManager: CLLocationManager!
    @IBOutlet weak var geolocImage: UIImageView!
    var delegate : TutoVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !animating{
            launchAnimation()
            
            //Zoom
            UIView.animate(withDuration: 9) {
                self.topConstraint.constant = -80
                self.rightConstraint.constant = -80
                
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func launchAnimation(){
        animating = true
        let count = 4
        let duration = 2.5
        
        for i in 0..<count{
            launchCircle(delay: Double(i) * duration/Double(count), duration: duration)
        }
    }
    
    
    func launchCircle(delay: Double, duration : Double){
        let radius = geolocImage.bounds.size.width / 2
        let circle = UIView.init(frame: geolocImage.bounds)
        circle.layer.cornerRadius = radius
        circle.layer.borderWidth = 2
        circle.layer.borderColor = UIColor.white.cgColor
        circle.backgroundColor = UIColor.clear
        circle.layer.masksToBounds = true
        
        geolocImage.addSubview(circle)
        circle.alpha = 0.8
        circle.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        UIView.animate(withDuration: duration, delay: delay, options: [.repeat, .beginFromCurrentState], animations: {
            
            circle.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
            circle.alpha = 0
        }, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onLocation(_ sender: Any) {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
}

extension LocationAskVC : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                    delegate.locationValidate()
                }
            }
        }
    }
}
