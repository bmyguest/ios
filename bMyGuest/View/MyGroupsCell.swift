//
//  MyGroupsCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class MyGroupsCell: UITableViewCell {

    @IBOutlet weak var imageGroup: UIImageView!
    @IBOutlet weak var membersCount: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(image: UIImage, members: [User], title: String, desc: String){
        imageGroup.image = image
        titleLabel.text = title
        membersCount.text = "\(members.count)"
        descLabel.text = desc
    }
    
}
