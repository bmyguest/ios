//
//  ProfileVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 13/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import Alamofire
import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nothingLabel: UILabel!
    @IBOutlet weak var groupButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var bottomLineConstraint: NSLayoutConstraint!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    @IBOutlet weak var firstnameTf: UITextField!
    @IBOutlet weak var phoneTf: UITextField!
    @IBOutlet weak var mailTf: UITextField!
    @IBOutlet weak var birthdateTf: UITextField!
    @IBOutlet weak var AddressTf: UITextField!
    @IBOutlet weak var lastnameTf: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var addFriend: UIButton!
    
    
    var user = UserManager.sharedInstance.user
    var allGroups : [[Group]] = []

    var originBottomLineConstraint : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.allowsMultipleSelectionDuringEditing = false
        
        if self.navigationController?.viewControllers.count == 1{
            backButton.setImage(UIImage(named: "cancel"), for: .normal)
            backButton.tag = 1
        }
        
        if user.id != UserManager.sharedInstance.user.id{
            anotherProfileSetup()
        }else{
            addFriend.isHidden = true
        }

        activityIndicator.hidesWhenStopped = true
        
        originBottomLineConstraint = bottomLineConstraint.constant
        
        tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        
        if let image = user.image{
            self.imageView.image = image
            self.imageView.clipsToBounds = true
            self.imageView.layer.cornerRadius = 80
            self.imageView.layer.borderWidth = 2
            self.imageView.layer.borderColor = UIColor.darkGray.cgColor//UIColor(red: 255.0/255.0, green: 68.0/255.0, blue: 105.0/255.0, alpha: 1.0).cgColor
        }
        
        if let gender = user.gender{
            if gender == "male"{
                genderSegment.selectedSegmentIndex = 0
            }else if gender == "female"{
                genderSegment.selectedSegmentIndex = 1
            }
        }
        
        if let name = user.name{
            nameTf.text = name
        }
        
        if let location = user.location{
            AddressTf.text = location
        }
        
        if let birthdate = user.birthday{
            let date = AppManager.sharedInstance.birthdateformatter.date(from: birthdate)
            birthdateTf.text = AppManager.sharedInstance.frBirthdateformatter.string(from: date!)
        }
        
        if let mail = user.email{
            mailTf.text = mail
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        self.navigationController?.navigationBar.isHidden = true
        activityIndicator.startAnimating()
        user.getApiUser(profileVC: self)
    }
    
    func anotherProfileSetup(){
        
        addFriend.isHidden = UserManager.sharedInstance.user.isFriendWith(id: self.user.id!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setButtonSelected(button: UIButton){
        if button.tag == 0{
            tableView.tag = 0
            nothingLabel.isHidden = user.contacts.count > 0
            nothingLabel.text = "Aucun contact"
            bottomLineConstraint.constant = originBottomLineConstraint
            contactButton.backgroundColor = UIColor.black.withAlphaComponent(0.1)
            groupButton.backgroundColor = UIColor.black.withAlphaComponent(0.05)
        }else{
            tableView.tag = 1
            nothingLabel.isHidden = user.groups.count > 0
            nothingLabel.text = "Aucun groupe"
            bottomLineConstraint.constant = 0
            contactButton.backgroundColor = UIColor.black.withAlphaComponent(0.05)
            groupButton.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        }
        tableView.reloadData()
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    //  Events
    @IBAction func onBack(_ sender: Any) {
        if backButton.tag == 1 {
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onValidate(_ sender: Any) {
        
        if validateButton.titleLabel?.text == "OK"{
            dismiss(animated: true, completion: nil)
        }
        
    }

    @IBAction func onContact(_ sender: UIButton) {
        setButtonSelected(button: sender)
    }
    
    @IBAction func onGroups(_ sender: UIButton) {
        setButtonSelected(button: sender)
    }
    
    @IBAction func onAddFriend(_ sender: Any) {
        UserManager.sharedInstance.user.followUser(user: self.user)
        addFriend.isHidden = true
    }
}

extension ProfileVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 1 {
            
            allGroups.removeAll()
            allGroups.append(user.groups)
            allGroups.append(user.memberships)
            
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView.tag == 1{
            if allGroups[section].count == 0 {
                return 0
            }
            return 20
        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            nothingLabel.isHidden = user.memberships.count > 0
            
            return allGroups[section].count
        }
        nothingLabel.isHidden = user.contacts.count > 0
        return user.contacts.count
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Mes groupes"
        }
        
        return "Groupe membre"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        if tableView.tag == 1 {
            cell.setup(group: allGroups[indexPath.section][indexPath.row])
        }
        if tableView.tag == 0 {
            cell.setup(user: user.contacts[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            let groupVC = GroupVC()

            groupVC.group = allGroups[indexPath.section][indexPath.row]
            groupVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(groupVC, animated: true)
            
            return
        }
        
        let pushProfileVC = ProfileVC()
        let pushUser = self.user.contacts[indexPath.row]
        
        if pushUser.id != UserManager.sharedInstance.user.id{
            pushUser.getApiUser(profileVC: pushProfileVC)
            pushProfileVC.user = pushUser
            pushProfileVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(pushProfileVC, animated: true)
        }
        
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        
//        return self.user.id == UserManager.sharedInstance.user.id!
//    }
//  
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        
//        if tableView.tag == 0{
//            let deleteAction = UITableViewRowAction(style: .normal, title: "Supprimer") { (rowaction, indexPath) in
//                self.user.unFollowUser(id: self.user.contacts[indexPath.row].id!)
//                UserManager.sharedInstance.user.contacts.remove(at: indexPath.row)
//                self.tableView.reloadData()
//            }
//            
//            deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
//            
//            return [deleteAction]
//        }
//        
//        let deleteAction = UITableViewRowAction(style: .normal, title: "Quitter") { (rowaction, indexPath) in
//            //Quitter groupe
//            self.tableView.reloadData()
//        }
//        
//        deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
//        
//        return [deleteAction]
//    }
}
