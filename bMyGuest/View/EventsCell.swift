//
//  EventsCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class EventsCell: UITableViewCell {

    @IBOutlet weak var nmParticipantsLAbel: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var restLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(event: Event){
        self.titleLabel.text = event.name_
        self.startLabel.text = "de " + AppManager.sharedInstance.hourFormatter.string(from: event.start_)
        self.endLabel.text = "à " + AppManager.sharedInstance.hourFormatter.string(from: event.end_)
        self.eventImage.image = event.picture_
        self.nmParticipantsLAbel.text = "\(event.participants_.count)"
        self.restLabel.text = event.capacity_ - event.participants_.count > 0 ? "\(event.capacity_ - event.participants_.count) places restantes" : "Complet"
        self.descLabel.text = event.description_
    }
    
}
