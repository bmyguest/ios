//
//  ResultsCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class ResultsCell: UITableViewCell {

    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var capacityLabel: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var participantsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(event: Event){
        self.title.text = event.name_
        self.startLabel.text = AppManager.sharedInstance.hourFormatter.string(from: event.start_) + " - " + AppManager.sharedInstance.hourFormatter.string(from: event.end_)
        self.capacityLabel.text = event.capacity_ > 0 ? event.capacity_.description : "∞"
        if event.picture_ == nil {
            self.imageEvent.alpha = 0.3
            self.imageEvent.image = #imageLiteral(resourceName: "party")
        }else{
            self.imageEvent.alpha = 1
            self.imageEvent.image = event.picture_
        }
        self.participantsLabel.text = "\(event.participants_.count) participants"
    }
}
