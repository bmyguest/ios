//
//  MenuVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var user : User!
    
    var menuContainers : [(section: String,array:[(image: UIImage, name: String, infos: String)])] = []
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.title = "Menu"
        self.tabBarItem.image = UIImage(named: "menu")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
    }
    
    func setupMenu(user: User){
        var section1 : [(image: UIImage, name: String, infos: String)] = []
        section1.append((image: user.image!, name: user.name!, infos: ""))
        
        var section2 : [(image: UIImage, name: String, infos: String)] = []
        section2.append((image: #imageLiteral(resourceName: "menu-my-events"), name: "Mes évenements", infos: "\(user.events.count)"))
        section2.append((image: #imageLiteral(resourceName: "menu-group"), name: "Mes groupes", infos: "\(user.groups.count)"))
        
        var section3 : [(image: UIImage, name: String, infos: String)] = []
        section3.append((image: #imageLiteral(resourceName: "menu-patrticipations"), name: "Participations", infos: "\(user.participations.count)"))
        section3.append((image: #imageLiteral(resourceName: "menu-friends"), name: "Contacts", infos: "\(user.contacts.count)"))
        section3.append((image: #imageLiteral(resourceName: "menu-members"), name: "Groupes membre", infos: "\(user.memberships.count)"))
        
        var section4 : [(image: UIImage, name: String, infos: String)] = []
        section4.append((image: #imageLiteral(resourceName: "menu-create-group"), name: "Créer un groupe", infos: ""))
        section4.append((image: #imageLiteral(resourceName: "menu-create-events"), name: "Créer un évenement", infos: ""))
        
        var section5 : [(image: UIImage, name: String, infos: String)] = []
        section5.append((image: #imageLiteral(resourceName: "menu-disconnect"), name: "Déconnexion", infos: ""))
        
        self.menuContainers = [("PROFIL", section1), ("GÉRER", section2), ("RACCOURCIS", section3), ("CRÉATION", section4), ("", section5)]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            UserManager.sharedInstance.user.getApiUser(profileVC: nil)
            self.user = UserManager.sharedInstance.user
            if self.user.image != nil{
                timer.invalidate()
                self.setupMenu(user: self.user)
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MenuVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let container = UIView()
        
        let label = UILabel(frame: CGRect(x: 10, y: 30, width: self.view.bounds.width, height: 20))
        label.text = menuContainers[section].section
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.init(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1)
        
        container.addSubview(label)
        
        return container
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuContainers.count
    }
    
    //Header
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuContainers[section].array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        let row = menuContainers[indexPath.section].array[indexPath.row]
        
        cell.setup(image: row.image, name: row.name, info: row.infos)
        
        cell.topLine.isHidden = indexPath.row > 0
        cell.bottomLineConstraint.constant = indexPath.row == menuContainers[indexPath.section].array.count - 1 ? 0 : 50
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section, indexPath.row) {
        //
        case(0,0):
            let profileVC = ProfileVC()
            profileVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(profileVC, animated: true)
         //
        case(1,1):
            let myGroupsVC = MyGroupsVC()
            myGroupsVC.groups = user.groups
            myGroupsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myGroupsVC, animated: true)
            break
            
        case(1,0):
            let myEventsVC = UserEventsVC()
            myEventsVC.isMyEvents = true
            myEventsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myEventsVC, animated: true)
            break
        //
        case(2,0):
            let userEventsVC = UserEventsVC()
            userEventsVC.isMyEvents = false
            userEventsVC.events = user.participations
            userEventsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(userEventsVC, animated: true)
            break
            
        case(2,1):
            let contactsVC = ContactsVC()
            contactsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(contactsVC, animated: true)
            break
            
        case(2,2):
            let myGroupsVC = MyGroupsVC()
            myGroupsVC.groups = user.memberships
            myGroupsVC.isMyGroups = false
            myGroupsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myGroupsVC, animated: true)
            break
        //
        case(3,0):
            let createGroupVC = CreateGroupVC()
            createGroupVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(createGroupVC, animated: true)
            break
            
        case(3,1):
            let createVC = CreateVC()
            createVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(createVC, animated: true)
            break
            
        case(4,0):
            //disconnect
            present(ConnectionVC(), animated: true, completion: nil)
            break
            
        default:
            break
        }
        
    }
}
