//
//  MyGroupsVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import Alamofire
import UIKit

class MyGroupsVC: UIViewController {

    @IBOutlet weak var noGroupsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var timedOutTimer : Timer?
    var groups : [Group] = []
    var isMyGroups = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isMyGroups{
            self.title = "Mes groupes"
            getMyGroups()
        }else{
            //groupe membre
            self.title = "Groupes membre"
            getMemberships()
        }

        tableView.register(UINib.init(nibName: "MyGroupsCell", bundle: nil), forCellReuseIdentifier: "MyGroupsCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMyGroups(){
        let url = "\(Config.baseURL)users/\(UserManager.sharedInstance.user.id!)/groups"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                if let jsons = response.result.value as? [[String:Any]]{
                    self.groups.removeAll()
                    for json in jsons{
                        let group = Group(json: json)
                        self.timedOutTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                            if group.image != nil {
                                timer.invalidate()
                                self.groups.append(group)
                                self.tableView.reloadData()
                                print("finish")
                            }else{
                                print("not yet")
                            }
                        })
                    }
                    self.noGroupsLabel.isHidden = self.groups.count > 0 ? true : false
                }
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
            }
        }
    }
    
    func deleteGroup(group: Group){
        let url = "\(Config.baseURL)groups/\(group.id!)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        print(parameters)
        
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }
    
    func quitGroup(group: Group){
        let url = "\(Config.baseURL)groups/\(group.id!)/members"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        print(parameters)
        
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                print(response.result)
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
                
            }
        }
    }
    
    func getMemberships(){
        let url = "\(Config.baseURL)users/\(UserManager.sharedInstance.user.id!)/memberships"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                if let jsons = response.result.value as? [[String:Any]]{
                    self.groups.removeAll()
                    for json in jsons{
                        let group = Group(json: json)
                        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                            if group.image != nil {
                                timer.invalidate()
                                self.groups.append(group)
                                self.tableView.reloadData()
                                print("finish")
                            }else{
                                print("not yet")
                            }
                        })
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
            }
        }
    }
}

extension MyGroupsVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MyGroupsCell = tableView.dequeueReusableCell(withIdentifier: "MyGroupsCell", for: indexPath) as! MyGroupsCell
        
        let group = groups[indexPath.row]
        
        cell.setup(image: group.image ?? #imageLiteral(resourceName: "cheers"), members: group.members, title: group.name!, desc: group.desc!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let timer = timedOutTimer{
            timer.invalidate()
        }
        let groupVC = GroupVC()
        groupVC.group = groups[indexPath.row]
        groupVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(groupVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if isMyGroups{
            let deleteAction = UITableViewRowAction(style: .normal, title: "Supprimer ce groupe") { (rowaction, indexPath) in
                self.deleteGroup(group: self.groups[indexPath.row])
                self.groups.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            
            deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
            
            return [deleteAction]
        }else{
            let deleteAction = UITableViewRowAction(style: .normal, title: "Quitter") { (rowaction, indexPath) in
                self.quitGroup(group: self.groups[indexPath.row])
                self.groups.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            
            deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
            
            return [deleteAction]
        }
        
    }
}


