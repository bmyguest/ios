//
//  ResultsVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import GooglePlaces

class ResultsVC: UIViewController {
    
    var events = [Event]()
    var refreshControl: UIRefreshControl!
    var countRequests = 0
    var latitude = 0.0
    var longitude = 0.0
    
    var city = "Paris"
    var range = "15 km"
    
    var radius = 150000
    
    var timeout = 0.0
    var timeoutTimerLocation : Timer?
    
    let locationManager = CLLocationManager()
    let autocompleteController = GMSAutocompleteViewController()

    @IBOutlet weak var cancelSearchBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searcheBArConstaintRight: NSLayoutConstraint!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var TopfilterViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var rangeSlider: UISlider!
    @IBOutlet weak var adressTf: UITextField!
    @IBOutlet weak var geolocImage: UIImageView!
    @IBOutlet weak var loadLocationView: UIView!
    @IBOutlet weak var topBarButton: UIButton!
    @IBOutlet weak var noEvents: UIView!
    @IBOutlet weak var noConnectionView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var resultsTableView: UITableView!

     
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.title = "Explorer"
        self.tabBarItem.image = UIImage(named: "search")
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        searcheBArConstaintRight.constant = self.view.bounds.width
        cancelSearchBtn.alpha = 0
        
        //Google places
        autocompleteController.delegate = self
        
        let filter = GMSAutocompleteFilter()
        filter.country = "fr"
        autocompleteController.autocompleteFilter = filter
        
        TopfilterViewConstraint.constant = -135
        activityIndicator.hidesWhenStopped = true
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        resultsTableView.addSubview(refreshControl)
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 3
        
        locationManager.startUpdatingLocation()
        
        resultsTableView.register(UINib(nibName: "ResultsCell", bundle: nil), forCellReuseIdentifier: "ResultsCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        launchAnimation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.navigationController?.navigationBar.isHidden = true
        self.resultsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(sender:AnyObject) {
        //refresh tableView
        self.callEvents(search: "")
    }
    
    func isFirstsImagesDownloded(number: Int) -> Bool{
        for i in 0..<number+1{
            if events[i].picture_ == #imageLiteral(resourceName: "party") || events[i].picture_ == nil{
                return false
            }
        }
        
        return true
    }
    
    func verifRadius(event: Event, radius: Int) -> Bool{
        
        let userLoc = CLLocation(latitude: CLLocationDegrees.init(self.latitude), longitude: CLLocationDegrees.init(self.longitude))
        let eventLoc = CLLocation(latitude: CLLocationDegrees.init(event.latitude_), longitude: CLLocationDegrees.init(event.longitude_))
        
        let distance = userLoc.distance(from: eventLoc)
        //print(distance)
        
        return Int(distance) <= radius
    }
    
    func callEvents(search: String){
        if !refreshControl.isRefreshing{
            activityIndicator.startAnimating()
        }
        
        let url = search.characters.count > 1 ? "\(Config.baseURL)events?search=" + search : "\(Config.baseURL)events"
        
        Alamofire.request(url).validate().responseJSON { response in
            
            guard let responseJSON = response.result.value as? [[String: Any]] else {
                print("error retreiving events")
                if self.countRequests < 5 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.callEvents(search: "")
                        self.countRequests += 1
                    })
                }else{
                    print("no connection")
                    self.loadLocationView.isHidden = true
                    self.activityIndicator.stopAnimating()
                    self.noConnectionView.isHidden = false
                }
                
                return
            }
            self.noConnectionView.isHidden = true
            self.countRequests = 0
            self.events.removeAll()
            
            for json in responseJSON {
                let event = Event.init(json: json)
                event.delegate = self
                if self.verifRadius(event: event, radius: self.radius){
                    self.events.append(event)
                }
            }
            
            self.noEvents.isHidden = self.events.count > 0
            
            //Wait for two first picture  (with timeout if too long)
            Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { (timer) in
                self.timeout += 0.2
                if self.events.count > 2{
                    if self.isFirstsImagesDownloded(number: 2) || self.timeout >= 20{
                        timer.invalidate()
                        self.loadLocationView.isHidden = true
                    }
                }else{
                    self.loadLocationView.isHidden = true
                }
            }
            
            self.refreshControl.endRefreshing()
            self.resultsTableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func launchAnimation(){
        let count = 4
        let duration = 2.5
        
        for i in 0..<count{
            launchCircle(delay: Double(i) * duration/Double(count), duration: duration)
        }
    }
    
    
    func launchCircle(delay: Double, duration : Double){
        let radius = geolocImage.bounds.size.width / 2
        let circle = UIView.init(frame: geolocImage.bounds)
        circle.layer.cornerRadius = radius
        circle.layer.borderWidth = 2
        circle.layer.borderColor = UIColor.white.cgColor
        circle.backgroundColor = UIColor.clear
        circle.layer.masksToBounds = true
        
        geolocImage.addSubview(circle)
        circle.alpha = 0.8
        circle.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        UIView.animate(withDuration: duration, delay: delay, options: [.repeat, .beginFromCurrentState], animations: {
            
            circle.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
            circle.alpha = 0
        }, completion: nil)
    }
    
    //EVENTS
    @IBAction func rangeValueChanged(_ sender: UISlider) {
        range = "\(Int(sender.value * 10)) km"
        radius = Int(sender.value * 10000)
        rangeLabel.text = range
    }
    
    @IBAction func onRelocation(_ sender: Any) {
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func onValideFilters(_ sender: Any) {
        self.callEvents(search: "")
        arrowImage.image = UIImage(named: "down-arrow")
        self.topBarButton.setTitle(city + " - " + range, for: .normal)
        TopfilterViewConstraint.constant = -135
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func onTopBarButton(_ sender: Any) {
        if TopfilterViewConstraint.constant == -135 {
            arrowImage.image = UIImage(named: "up-arrow")
            TopfilterViewConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else{
            TopfilterViewConstraint.constant = -135
            arrowImage.image = UIImage(named: "down-arrow")
        }
    }
    
    @IBAction func onCancelSearch(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.searchBar.text = ""
            self.searcheBArConstaintRight.constant = self.view.bounds.width
            self.cancelSearchBtn.alpha = 0
            self.searchBar.resignFirstResponder()
            self.callEvents(search: "")
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func onSearch(_ sender: Any) {
        UIView.animate(withDuration: 0.4) {
            self.searcheBArConstaintRight.constant = 8
            self.cancelSearchBtn.alpha = 0.8
            self.searchBar.becomeFirstResponder()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func onProfile(_ sender: Any) {
//        let profileVC = ProfileVC()
//        //UserManager.sharedInstance.user.getApiUser(profileVC: profileVC)
//        let navProfile = UINavigationController(rootViewController: profileVC)
//        present(navProfile, animated: true, completion: nil)
    }
    
    @IBAction func onReload(_ sender: Any) {
        countRequests = 0
        loadLocationView.isHidden = false
        noConnectionView.isHidden = true
        self.callEvents(search: "")
    }
    
}

extension ResultsVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            if let pms = placemarks{
                if pms.count > 0 /*&& self.adressTf.text?.characters.count == 0*/{
                    
                    if let timerTimeout = self.timeoutTimerLocation{
                        timerTimeout.invalidate()
                    }
                    
                    let pm = pms[0] as CLPlacemark
                    self.topBarButton.setTitle(pm.locality! + " - " + self.range, for: .normal)
                    
                    if let addressDictionary = pm.addressDictionary{
                        let formattedAddressLines = addressDictionary["FormattedAddressLines"] as! [String]
                        self.adressTf.text = formattedAddressLines[0] + ", " + formattedAddressLines[1]
                        self.city = formattedAddressLines[1]
                    }
                
                    self.latitude = (pm.location?.coordinate.latitude)!
                    self.longitude = (pm.location?.coordinate.longitude)!

                    self.callEvents(search: "")
                    self.locationManager.stopUpdatingLocation()
                }
            }else{
                self.loadLocationView.isHidden = true
                //self.noConnectionView.isHidden = false
            }
        }
    }
}

extension ResultsVC : UITableViewDataSource, UITableViewDelegate{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as! ResultsCell
        
        cell.setup(event: self.events[indexPath.row])
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventVC = EventViewController()
        eventVC.event = events[indexPath.row]
        eventVC.hidesBottomBarWhenPushed = true
        eventVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(eventVC, animated: true)
    }
}

extension ResultsVC : EventDelegate {
    
    func publicAddressFound(publicAddress: String) {
        
    }
    
    func pictureDownloaded(image: UIImage) {
        self.resultsTableView.reloadData()
    }
}

extension ResultsVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        present(autocompleteController, animated: true, completion: nil)
    }
}

extension ResultsVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if let address = place.formattedAddress{
            self.adressTf.text = address
        }
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        print(latitude)
        print(longitude)
        self.city = "\(place.addressComponents?[2].name ?? "")"
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension ResultsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.callEvents(search: searchText.replacingOccurrences(of: " ", with: "%20"))
    }
}
