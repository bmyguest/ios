//
//  GroupVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 22/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire

class GroupVC: UIViewController {
    
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var titleLAbel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var membersLabel: UILabel!
    @IBOutlet weak var mediasBtn: UIButton!
    @IBOutlet weak var membersBtn: UIButton!
    @IBOutlet weak var bottomLineConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var joinGroupBtn: UIButton!
    
    var group : Group?
    
    var originBottomLineConstraint : CGFloat = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        originBottomLineConstraint = bottomLineConstraint.constant

        ownerImage.layer.borderColor = UIColor.white.cgColor
        ownerImage.layer.borderWidth = 1
        
        tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        collectionView.register(UINib(nibName: "GroupCell", bundle: nil), forCellWithReuseIdentifier: "GroupCell")
        
        if let group = group{
            refreshUI()
            getGroup(id: group.id!)
        }
    }
    
    func imageLoaded() -> Bool{
        if let medias = group?.medias{
            for media in medias{
                if media.image == nil{
                    return false
                }
            }
        }
        
        return true
    }
    
    func refreshUI(){
        
//        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
//            if self.imageLoaded(){
//                self.collectionView.reloadData()
//                timer.invalidate()
//            }
//        }
        
        if let group = self.group{
            joinGroupBtn.isEnabled = true
            joinGroupBtn.alpha = 1
            for member in group.members{
                if member.id == UserManager.sharedInstance.user.id{
                    joinGroupBtn.isEnabled = false
                    joinGroupBtn.alpha = 0.5
                }
            }
            
            if let name = group.name{
                titleLAbel.text = name
            }
            
            if let owner = group.owner{
                subTitle.text = "de " + owner.name!
            }
            
            if let image = group.image{
                groupImage.image = image
            }
            if let desc = group.desc {
                groupDescription.text = desc
            }
            if let user = group.owner{
                if let image = user.image{
                    ownerImage.image = image
                }
            }
            
            membersLabel.text = "\(group.members.count)"
        }
    }
    
    func putToFirst(media: Media){
        if let group = self.group{
            var newMedias : [Media] = []
            
            newMedias.append(media)
            
            for groupMedia in group.medias{
                if groupMedia.id != media.id{
                    newMedias.append(groupMedia)
                }
            }
            self.group?.medias = newMedias
        }
    }
    
    func getGroup(id : Int){
        let url = "\(Config.baseURL)groups/\(id)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                if let json = response.result.value as? [String:Any]{
                    self.group = Group(json: json)
                    
                    //wait for owner image
                    Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                        if self.group?.owner?.image != nil {
                            timer.invalidate()
                            self.refreshUI()
                            print("finish")
                        }else{
                            print("not yet")
                        }
                    })
                    
                    //load each medias
                    for media in (self.group?.medias)!{
                        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true, block: { (timer) in
                            if media.image != nil{
                                timer.invalidate()
                                //self.putToFirst(media: media)
                                self.collectionView.reloadData()
                            }
                        })
                    }
                    
                }
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
            }
        }
    }
    
    func setButtonSelected(button: UIButton){
        if button.tag == 0{
            tableView.isHidden = true
            collectionView.isHidden = false
            bottomLineConstraint.constant = originBottomLineConstraint
            mediasBtn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            membersBtn.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        }else{
            collectionView.isHidden = true
            tableView.isHidden = false
            bottomLineConstraint.constant = 0
            membersBtn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            mediasBtn.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        }
        collectionView.reloadData()
        tableView.reloadData()
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

    
    //Events
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onJoinGroup(_ sender: UIButton) {
        sender.isEnabled = false
        if let group = self.group{
            let url = "\(Config.baseURL)groups/\(group.id!)/members"
            
            let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
            let parameters: Parameters = ["status": 0]
            
            //print(url)
            print(parameters)
            
            group.members.append(UserManager.sharedInstance.user)
            tableView.reloadData()
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                //print(response)
                
                switch(response.result) {
                case .success(_):
                    print(response.result)
                    break
                    
                case .failure(_):
                    print(response.result.error ?? "")
                    break
                    
                }
            }
        }
    }
    
    @IBAction func onMedias(_ sender: UIButton) {
        setButtonSelected(button: sender)
    }
    
    @IBAction func onMembers(_ sender: UIButton) {
        setButtonSelected(button: sender)
    }
    
    func imageTapped(_ sender: UIImage) {
        let newImageView = UIImageView(image: sender)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

}

extension GroupVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let group = self.group{
            return group.members.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        if let group = self.group{
              cell.setup(user: group.members[indexPath.row])
        }
        
        return cell
    }
}

extension GroupVC : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.bounds.width/2 - 5, height: self.view.bounds.width/2 - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
         //top left bottom right
        return UIEdgeInsetsMake(5, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let group = self.group{
            return group.medias.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : GroupCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCell", for: indexPath) as! GroupCell
        
        if let group = self.group{
            if let image = group.medias[indexPath.row].image{
                cell.image.alpha = 0
                cell.image.image = image
                UIView.animate(withDuration: 1, animations: {
                    cell.image.alpha = 1
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! GroupCell
        imageTapped(cell.image.image!)
    }
}
