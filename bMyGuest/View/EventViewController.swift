//
//  EventViewController.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 21/05/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire

class EventViewController: UIViewController {
    
    var event : Event?

    @IBOutlet weak var subDateLAbel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var capacityLabel: UILabel!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var ownerPicture: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var restLabel: UILabel!
    @IBOutlet weak var participantsCollectionView: UICollectionView!
    @IBOutlet weak var participateBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        participantsCollectionView.register(UINib(nibName: "ParticipantsCell", bundle: nil), forCellWithReuseIdentifier: "ParticipantsCell")
        
        
        if let event = self.event{
            
            self.title = event.name_
            eventImage.image = event.picture_

            refreshUI(event: event)

            getEvent(id: event.id)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func getEvent(id: Int){
        let url = "\(Config.baseURL)events/\(id)"
        
        let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
        let parameters: Parameters = [:]
        
        //print(url)
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { (response:DataResponse<Any>) in
            //print(response)
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value as? [String:Any]{
                    //print(data)
                    let event = Event(json: data)
                    event.delegate = self
                    self.refreshUI(event: event)
                }
                break
                
            case .failure(_):
                print(response.result.error ?? "")
                break
            }
        }
    }
    
    func refreshUI(event: Event){
        self.participantsCollectionView.reloadData()
        
        participantsLabel.text = event.participants_.count > 1 ? "\(event.participants_.count) participants" : "\(event.participants_.count) participant"
        let rest = event.capacity_ - event.participants_.count
        restLabel.text = rest > 0 ? "Il reste \(rest) places" : "Évenement complet"
        
        if rest < 1{
            participateBtn.isEnabled = false
            participateBtn.alpha = 0.5
            participateBtn.setTitle("Complet", for: .normal)
        }
        if event.publicAddress.characters.count > 0{
            cityLabel.text = event.publicAddress
        }
        
        for user in event.participants_{
            if user.id == UserManager.sharedInstance.user.id{
                participateBtn.isEnabled = true
                participateBtn.alpha = 1
                participateBtn.setTitle("Ne plus participer", for: .normal)
                participateBtn.tag = 1
            }
        }

        var startDayAndMonth = AppManager.sharedInstance.dayAndMonthDateTimeFormatter.string(from: event.start_)
        var endDayAndMonth = AppManager.sharedInstance.dayAndMonthDateTimeFormatter.string(from: event.end_)
        let year = Calendar.current.component(.year, from: Date()).description
        startDayAndMonth = startDayAndMonth.replacingOccurrences(of: year, with: "")
        endDayAndMonth = endDayAndMonth.replacingOccurrences(of: year, with: "")
        
        dateLabel.text = startDayAndMonth
            + "- "
            + endDayAndMonth
        
        subDateLAbel.text = "Du " + startDayAndMonth
            + "à "
            + AppManager.sharedInstance.timeDateTimeFormatter.string(from: event.start_)
            + " au "
            + endDayAndMonth
            + "à "
            + AppManager.sharedInstance.timeDateTimeFormatter.string(from: event.end_)
        
        nameLabel.text = event.name_
        capacityLabel.text = event.capacity_.description
        descriptionTV.text = event.description_
        subTitleLabel.text = "par " + event.owner_.name!
        if let image = event.owner_.image{
            ownerPicture.image = image
            ownerPicture.layer.borderColor = UIColor.white.cgColor
            ownerPicture.layer.borderWidth = 1
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    //EVENTS
    @IBAction func onParticipate(_ sender: Any) {
        
        if participateBtn.tag == 0{
            
            //Go to event
            if let event = self.event{
                let url = "\(Config.baseURL)events/\(event.id)/participants"
                
                let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
                let parameters: Parameters = ["status": 0]
                
                //print(url)
                print(parameters)
                
                if (self.event?.participants_.count)! > 0{
                    self.event?.participants_.insert(UserManager.sharedInstance.user, at: 1)
                }else{
                    self.event?.participants_.append(UserManager.sharedInstance.user)
                }
                self.participantsCollectionView.reloadData()
                self.participateBtn.setTitle("Ne plus participer", for: .normal)
                self.participateBtn.tag = 1
                
                Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                    //print(response)
                    
                    switch(response.result) {
                    case .success(_):
                        print(response.result)
                        self.getEvent(id: event.id)
                        break
                        
                    case .failure(_):
                        print(response.result.error ?? "")
                        break
                        
                    }
                }
            }
        }else{
            //Don't go to event
            if let event = self.event{
                let url = "\(Config.baseURL)events/\(event.id)/participants/\(UserManager.sharedInstance.user.id!)"
                
                let headers: HTTPHeaders = ["Authorization": UserManager.sharedInstance.user.token!]
                let parameters: Parameters = [:]
                
                //print(url)
                print(parameters)
                
                var i = 0
                for participant in (self.event?.participants_)!{
                    if participant.id! == UserManager.sharedInstance.user.id!{
                        self.event?.participants_.remove(at: i)
                    }
                    i += 1
                }
                self.participantsCollectionView.reloadData()
                self.participateBtn.setTitle("Participer", for: .normal)
                self.participateBtn.tag = 0
                
                Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                    //print(response)
                    
                    switch(response.result) {
                    case .success(_):
                        print(response.result)
                        self.getEvent(id: (self.event?.id)!)
                        break
                        
                    case .failure(_):
                        print(response.result.error ?? "")
                        break
                        
                    }
                }
            }
        }
    }
}

extension EventViewController : EventDelegate {
    func pictureDownloaded(image: UIImage) {
        eventImage.image = image
    }
    
    func publicAddressFound(publicAddress: String) {
        cityLabel.text = publicAddress
    }
}

extension EventViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 15, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 73, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if let event = self.event{
            return event.participants_.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let event = self.event{
            
            let profileVC = ProfileVC()
            profileVC.user = event.participants_[indexPath.row]
            profileVC.hidesBottomBarWhenPushed = true
            if event.participants_[indexPath.row].id != UserManager.sharedInstance.user.id{
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantsCell", for: indexPath) as! ParticipantsCell
        
        if let event = self.event{
            if let image = event.participants_[indexPath.row].image{
                cell.profileImage.image = image
            }
            if let name = event.participants_[indexPath.row].name {
                if let firstname = name.components(separatedBy: " ").first{
                    
                    cell.infoLabel.text = firstname
                    cell.ageLabel.text = event.participants_[indexPath.row].age.description + " ans"
                }
            }
        }

        return cell
    }
    
}
