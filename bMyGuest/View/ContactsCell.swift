//
//  ContactsCell.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(image : UIImage, name: String, location : String, age: Int){
        
        userImage.image = image
        nameLabel.text = name
        locationLabel.text = location
        if age > 0{
            ageLabel.text = "\(age) ans"
        }else{
            ageLabel.isHidden = true
        }
    }
    
}
