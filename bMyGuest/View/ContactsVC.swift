//
//  ContactsVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 23/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import Alamofire

class ContactsVC: UIViewController {

    
    @IBOutlet weak var noContactLabel: UILabel!
    var contacts : [User] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Mes contacts"
        
        tableView.register(UINib(nibName: "ContactsCell", bundle: nil), forCellReuseIdentifier: "ContactsCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        contacts = UserManager.sharedInstance.user.contacts
        noContactLabel.isHidden = contacts.count > 0
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ContactsVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ContactsCell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as! ContactsCell
        
        let contact = contacts[indexPath.row]
        
        cell.setup(image: contact.image ?? #imageLiteral(resourceName: "people"), name: contact.name!, location: contact.location!, age: contact.age)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = ProfileVC()
        profileVC.user = self.contacts[indexPath.row]
        profileVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Supprimer") { (rowaction, indexPath) in
            UserManager.sharedInstance.user.unFollowUser(id: self.contacts[indexPath.row].id!)
            self.contacts.remove(at: indexPath.row)
            UserManager.sharedInstance.user.contacts.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
        
        deleteAction.backgroundColor = AppManager.sharedInstance.mainColor
        
        return [deleteAction]
    }

}
