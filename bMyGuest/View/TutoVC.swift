//
//  TutoVC.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import CoreLocation


class TutoVC: UIViewController {
    
    var index = 0
    @IBOutlet var myPageController: UIPageViewController!
    var myViewControllers : [UIViewController] = []
    @IBOutlet weak var dots: UIPageControl!
    
//    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]? = nil) {
//        
//        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        
        myPageController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
        myPageController.delegate = self
        myPageController.dataSource = self
        
        self.addChildViewController(self.myPageController)
        self.view.addSubview(myPageController.view)
        self.myPageController.didMove(toParentViewController: self)
        self.myPageController.view.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height + 37)
        
        
        //myViewControllers.append(PagesVC.init(nibName: "Page1", bundle: nil))
        myViewControllers.append(PagesVC.init(nibName: "PagesVC", bundle: nil))
        
        if !(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .denied){
            let locationAskVC = LocationAskVC()
            locationAskVC.delegate = self
            myViewControllers.append(locationAskVC)
        }else{
            myViewControllers.append(ConnectionVC())
        }
        
        
        myPageController.setViewControllers([myViewControllers.first!], direction: .forward, animated: false, completion: nil)
        
        self.view.bringSubview(toFront: dots)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension TutoVC : TutoVCDelegate{
    
    //location validate, allow to go to connectVC
    func locationValidate() {
        var i = 0
        for vc in myViewControllers{
            if vc is LocationAskVC{
                myViewControllers.remove(at: i)
                break
            }
            i += 1
        }
        myViewControllers.append(ConnectionVC())
        myPageController.setViewControllers([myViewControllers.last!], direction: .forward, animated: true, completion: nil)
    }
}

extension TutoVC : UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        
        index = myViewControllers.index(of: viewController)!
        dots.currentPage = index
        if index == 0{
            return nil
        }
        
        return myViewControllers[index-1]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        
        index = myViewControllers.index(of: viewController)!
        dots.currentPage = index
        if index == myViewControllers.count - 1{
            return nil
        }
        
        return myViewControllers[index+1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        dots.numberOfPages = myViewControllers.count
        return myViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
