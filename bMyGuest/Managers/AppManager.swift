//
//  AppManager.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 24/05/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    static let sharedInstance = AppManager()
    
//    var isConnected : Bool {
//        set {
//            UserDefaults.standard.set(newValue, forKey: "isConnected")
//        }
//        get {
//            return UserDefaults.standard.bool(forKey: "isConnected")
//        }
//    }
    
    let mainColor = UIColor(red: 255.0/255.0, green: 68.0/255.0, blue: 105.0/255.0, alpha: 1)
    
    var work = false
    
    lazy var longDateTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = NSLocale.current
        
        formatter.locale = locale
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        
        return formatter
    }()
    
    lazy var dayAndMonthDateTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = NSLocale.current
        
        formatter.locale = locale
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        
        return formatter
    }()
    
    lazy var timeDateTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = NSLocale.current
        
        formatter.locale = locale
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        
        return formatter
    }()
    
    lazy var shortDateTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = NSLocale.current
        
        formatter.locale = locale
        formatter.dateStyle = .short
        
        return formatter
    }()
    
    lazy var hourFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = NSLocale.current
        
        formatter.locale = locale
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        
        return formatter
    }()
    
    lazy var dateformatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return formatter
    }()
    
    lazy var birthdateformatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        return formatter
    }()
    
    lazy var frBirthdateformatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        return formatter
    }()
    
//    lazy var user : User = {
//        
//        let date = self.dateformatter.date(from: "1990-12-26T23:00:00.000Z")
//        
////        let user = User(id: 791, token: "0", firstname: "Kevin", lastname: "Konrad", email: "konrad.kevin78@gmail.com", birthday: date!, gender: "Male", phone: "+33609977022", address: "151, avenue Carnot 78700 FRANCE", isPremium: false, note: 7.5, picture: "https://avatars2.githubusercontent.com/u/6616846")
//        
//        let user = User(id: "791", token: "0", name: "Kevin Konrad", email: "konrad.kevin78@gmail.com", birthday: "28/09/1995", gender: "Male", location: "", isPremium: false, note: 7.5, picture: "https://avatars2.githubusercontent.com/u/6616846")
//        
//        return user
//    }()
    

}
