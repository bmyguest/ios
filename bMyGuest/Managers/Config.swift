//
//  Config.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 15/04/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import Foundation

struct Config {
    
    private static func retrieveAPI(key: String) -> Any? {
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            
            if let dic = NSDictionary(contentsOfFile: path) as? [String: Any] {
                let api = NSDictionary(dictionary: dic["API"] as! Dictionary)
                
                return api[key] as! String
            }
        }
        return nil
    }
    
    static var baseURL: String {
        return retrieveAPI(key: "BaseURL") as! String
    }
}
