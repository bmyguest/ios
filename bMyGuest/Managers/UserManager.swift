//
//  UserManager.swift
//  bMyGuest
//
//  Created by Alexandre Ménielle on 14/07/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class UserManager: NSObject {
    static let sharedInstance = UserManager()
    
    lazy var user : User = {
        
        return User()
    }()
    
    func getUserInfos(token : String){
        print(token)
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, gender, birthday, link, email, location, picture.type(large), events, friends"]).start(completionHandler: { (connection, result, error) in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                //print(fbDetails)
                
                let id = fbDetails["id"] as! String
                let name = fbDetails["name"] as! String
                let email = fbDetails["email"] as! String
                let gender = fbDetails["gender"] as! String
                var location = ""
                if let locationDic = fbDetails["location"] as? [String: Any]{
                    location = locationDic["name"] as! String
                }
                var picture = ""
                if let pictureDic = fbDetails["picture"] as? [String: Any]{
                    let data = pictureDic["data"] as! [String: Any]
                    picture = data["url"] as! String
                }
                let birthday = fbDetails["birthday"] as? String
                
                let user = User(id: id, token: token, name: name, email: email, birthday: birthday ?? "", gender: gender, location: location, isPremium: false, note: 7.5, picture: picture)
                UserManager.sharedInstance.user = user
                
            }else{
                print(error!)
            }
        })
    }
}
